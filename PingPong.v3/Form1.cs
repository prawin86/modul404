﻿using System;
using System.Drawing;
using System.Windows.Forms;

namespace PingPong.v3
{
    public partial class Form1 : Form
    {
        private int _directionX = 5;
        private int _directionY = 2;
        private int _points = 0;

        public Form1()
        {
            InitializeComponent();
        }

        // Startknopf + Timer starten
        private void btnStart_Click(object sender, EventArgs e)
        {
            tmrSpiel.Start();
        }

        // Im Laufzeit Timer
        private void tmrSpiel_Tick(object sender, EventArgs e)
        {
            // Ballbewegung X zu X, Y zu Y
            picBall.Location = new Point(picBall.Location.X + _directionX, picBall.Location.Y + _directionY);

            // Ball trifft auf rechten Spielfeldrand
            if (picBall.Location.X >= pnlSpiel.Width - picBall.Width
                && picBall.Location.Y + picBall.Height >= picSchlägerRechts.Location.Y
                && picBall.Location.Y <= picSchlägerRechts.Location.Y + picSchlägerRechts.Height)
            {
                // +10 Punkte 
                _directionX = -_directionX;
                _points += 10;
            }

            // Ball trifft auf linken Spielfeldrand
            if (picBall.Location.X <= 0)
            {
                _directionX = -_directionX;
            }

            // Ball trifft auf oberen Spielfeldrand
            if (picBall.Location.Y >= pnlSpiel.Height - picBall.Height)
            {
                _directionY = -_directionY;
            }

            // Ball trifft auf unteren Spielfeldrand
            if (picBall.Location.Y < 0)
            {
                _directionY = -_directionY;
            }

            {
                // Punkte anzeigen
                txtPunkte.Text = Convert.ToString(_points);
            }
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            // Schlächer am rechten Spielrand platzieren
            picSchlägerRechts.Location = new Point(pnlSpiel.Width - picSchlägerRechts.Width - picSchlägerRechts.Width,
                pnlSpiel.Height / 2);

            // Scrollleiste neben Feld, Einst. min/max und aktuellen Wert
            vsbSchlägerRechts.Height = pnlSpiel.Height;
            vsbSchlägerRechts.Location = new Point(pnlSpiel.Location.X + pnlSpiel.Width, pnlSpiel.Location.Y);
            vsbSchlägerRechts.Minimum = 0;
            vsbSchlägerRechts.Maximum = pnlSpiel.Height - picSchlägerRechts.Height + vsbSchlägerRechts.LargeChange;
            vsbSchlägerRechts.Value = picSchlägerRechts.Location.Y;
        }

        private void vsbSchlägerRechts_Scroll(object sender, ScrollEventArgs e)
        {
            // Schlögerposition mit Scrolleiste synchronisieren
            picSchlägerRechts.Location = new Point(picSchlägerRechts.Location.X, vsbSchlägerRechts.Value);
            vsbSchlägerRechts.Value = picSchlägerRechts.Location.Y;
        }
        
        // Muss eingefügt werden, damit die Pfeiltasten nicht von Steuerelement zu Steuerelement springen
        // Den Ball mit den Pfeiltasten kontrolloeren
        protected override bool ProcessDialogKey(Keys keyData)
        {
            if (keyData == Keys.Up)
            {

                picBall.Location = new Point(picBall.Location.X, picBall.Location.Y - 25);
                return true; // // Um zu verhindern, dass Focus weitergereicht wird
            }
            else if (keyData == Keys.Down)
            {
                picBall.Location = new Point(picBall.Location.X, picBall.Location.Y + 25);
                return true; // Um zu verhindern, dass Focus weitergereicht wird
            }
            else if (keyData == Keys.Left)
            {
                picBall.Location = new Point(picBall.Location.X - 25, picBall.Location.Y);
                return true;
            }
            else if (keyData == Keys.Right)
            {
                picBall.Location = new Point(picBall.Location.X + 25, picBall.Location.Y);
                return true;
            }

            return base.ProcessDialogKey(keyData);

        }
        private void btnHoch_Click_1(object sender, EventArgs e)
        {
            picBall.Location = new Point(picBall.Location.X, picBall.Location.Y - 25);
        }

        private void btnRechts_Click(object sender, EventArgs e)
        {
            picBall.Location = new Point(picBall.Location.X + 25, picBall.Location.Y);
        }

        private void btnLinks_Click(object sender, EventArgs e)
        {
        picBall.Location = new Point(picBall.Location.X - 25, picBall.Location.Y);
        }

        private void btnUnten_Click(object sender, EventArgs e)
        {
            picBall.Location = new Point(picBall.Location.X, picBall.Location.Y + 25);
        }
    }
}

