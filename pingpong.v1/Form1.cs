﻿using System;
using System.Drawing;
using System.Windows.Forms;

namespace PingPong.v1
{
    public partial class Form1 : Form
    {
        private int _directionX = 5;
        private int _directionY = 2;

        public Form1()
        {
            InitializeComponent();
        }

        //Spiel Starten
        private void btnStart_Click(object sender, EventArgs e)
        {
            tmrSpiel.Start();
        }

        private void EventHandler(object sender, EventArgs e)
        {
            // Ballbewegung
            picBall.Location = new Point(picBall.Location.X + _directionX, picBall.Location.Y + _directionY);

            // Ball trifft auf rechten Spielfeldrand
            if (picBall.Location.X >= pnlSpiel.Width - picBall.Width) _directionX = -_directionX;

            // Ball trifft auf linken Spielfeldrand
            if (picBall.Location.X <= 0) _directionX = -_directionX;

            // Ball trifft auf oberen Spielfeldrand
            if (picBall.Location.Y >= pnlSpiel.Height - picBall.Height) _directionY = -_directionY;

            // Ball trifft auf unteren Spielfeldrand
            if (picBall.Location.Y < 0) _directionY = -_directionY;
        }
    }
}