﻿using System;
using System.Drawing;
using System.Windows.Forms;

namespace Schachbrett
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            // Komponenten initialisieren
            InitializeComponent();
        }

        // Start-Knopf
        private void btnStart_Click(object sender, EventArgs e)
        {
            AufbauSchachbrett(Convert.ToInt32(nudAnzahlZeilen.Value));
        }

        // Rechenoperation: Schachbrett / Anzahl Felder
        private void AufbauSchachbrett(int anzahl)
        {
            var panelbreite = pnlSchachbrett.Width;
            var panelhoehe = pnlSchachbrett.Height;
            var feldbreite = panelbreite / anzahl;
            var feldhoehe = panelhoehe / anzahl;
            var geradeZeilen = true;

            pnlSchachbrett.Controls.Clear(); // Aktuellen Schachbrett löschen
            for (var y = 0; y + feldhoehe <= panelhoehe; y = y + feldhoehe)
            {
                var startpunktX = geradeZeilen ? feldbreite : 0;
                // Wechsel auf die nächste Zeile
                geradeZeilen = !geradeZeilen;
                for (var x = startpunktX; x + feldbreite <= panelbreite; x = x + 2 * feldbreite)
                {
                    var feld = new Label
                    {
                        BackColor = Color.Black,
                        Location = new Point(x, y),
                        Width = feldbreite,
                        Height = feldhoehe
                    };
                    pnlSchachbrett.Controls.Add(feld);
                }
            }
        }
    }
}