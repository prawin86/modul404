﻿using System;
using System.Windows.Forms;

namespace Taschenrechner
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        // Form1 laden (Oberfläche)
        private void Form1_Load(object sender, EventArgs e)
        {
            //throw new System.NotImplementedException();
        }

        // Addition
        public void btnAddition_Click(object sender, EventArgs e)
        {
            var zahl1 = Convert.ToDouble(txtOperand1.Text);
            var zahl2 = Convert.ToDouble(txtOperand2.Text);
            var ergebnis = zahl1 + zahl2;
            lblErgebnis.Text = Convert.ToString(ergebnis);

            lblOperator.Text = "+";
        }

        // Subtraktion
        private void btnSubtraktion_Click_1(object sender, EventArgs e)
        {
            var zahl1 = Convert.ToDouble(txtOperand1.Text);
            var zahl2 = Convert.ToDouble(txtOperand2.Text);
            var ergebnis = zahl1 - zahl2;
            lblErgebnis.Text = Convert.ToString(ergebnis);

            lblOperator.Text = "-";
        }

        // Mittelwert (Durchschnitt)
        private void button2_Click(object sender, EventArgs e)
        {
            var zahl1 = Convert.ToDouble(txtOperand1.Text);
            var zahl2 = Convert.ToDouble(txtOperand2.Text);
            var ergebnis = (zahl1 + zahl2) / 2;
            lblErgebnis.Text = Convert.ToString(ergebnis);

            lblOperator.Text = "Ø";
        }

        // Potenz
        private void btnPotenz_Click(object sender, EventArgs e)
        {
            var zahl1 = Convert.ToDouble(txtOperand1.Text);
            var zahl2 = Convert.ToDouble(txtOperand2.Text);
            var ergebnis = Math.Pow(zahl1, zahl2);
            lblErgebnis.Text = Convert.ToString(ergebnis);

            lblOperator.Text = "^";
        }

        // Maximum (höchste Zahl) mit der Math.Max Funktion
        private void btnMaximum_Click(object sender, EventArgs e)
        {
            var zahl1 = Convert.ToDouble(txtOperand1.Text);
            var zahl2 = Convert.ToDouble(txtOperand2.Text);
            var ergebnis = Math.Max(zahl1, zahl2);
            lblErgebnis.Text = Convert.ToString(ergebnis);

            lblOperator.Text = "Max";
        }

        // Modulo
        private void btnModulo_Click(object sender, EventArgs e)
        {
            var zahl1 = Convert.ToDouble(txtOperand1.Text);
            var zahl2 = Convert.ToDouble(txtOperand2.Text);
            var ergebnis = zahl1 % zahl2;
            lblErgebnis.Text = Convert.ToString(ergebnis);

            lblOperator.Text = "mod";
        }


        // Division
        private void btnDivision_Click(object sender, EventArgs e)
        {
            var zahl1 = Convert.ToDouble(txtOperand1.Text);
            var zahl2 = Convert.ToDouble(txtOperand2.Text);
            var ergebnis = zahl1 / zahl2;
            lblErgebnis.Text = Convert.ToString(ergebnis);

            lblOperator.Text = "/";
        }

        // Minimum (kleinste Zahl) mit der Math.Max Funktion
        private void btnMinimum_Click(object sender, EventArgs e)
        {
            var zahl1 = Convert.ToDouble(txtOperand1.Text);
            var zahl2 = Convert.ToDouble(txtOperand2.Text);
            var ergebnis = Math.Min(zahl1, zahl2);
            lblErgebnis.Text = Convert.ToString(ergebnis);

            lblOperator.Text = "Min";
        }

        // Multiplikation
        private void btnMultiplikation_Click(object sender, EventArgs e)
        {
            var zahl1 = Convert.ToDouble(txtOperand1.Text);
            var zahl2 = Convert.ToDouble(txtOperand2.Text);
            var ergebnis = zahl1 * zahl2;
            lblErgebnis.Text = Convert.ToString(ergebnis);

            lblOperator.Text = "*";
        }
    }
}