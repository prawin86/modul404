﻿using System;
using System.Windows.Forms;

namespace StandardEreignisse
{
    public partial class Form2 : Form
    {
        private Form btnInfo;

        public Form2(Form parentForm)
        {
            // Komponenten initialisieren
            InitializeComponent();
            btnInfo = parentForm;
            lblAusgabe.Text += "Dies ist das Informationsformular" + Environment.NewLine; // Text für Label nach Abruf
        }
        
        // Knopf schliessen
        private void btnSchliessen_Click(object sender, EventArgs e)
        {
            Close();
        }
    }
}