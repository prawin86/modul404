﻿using System.ComponentModel;

namespace StandardEreignisse
{
    partial class Form2
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }

            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblAusgabe = new System.Windows.Forms.Label();
            this.btnSchliessen = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // lblAusgabe
            // 
            this.lblAusgabe.Location = new System.Drawing.Point(12, 121);
            this.lblAusgabe.Name = "lblAusgabe";
            this.lblAusgabe.Size = new System.Drawing.Size(290, 34);
            this.lblAusgabe.TabIndex = 0;
            // 
            // btnSchliessen
            // 
            this.btnSchliessen.Location = new System.Drawing.Point(250, 262);
            this.btnSchliessen.Name = "btnSchliessen";
            this.btnSchliessen.Size = new System.Drawing.Size(85, 31);
            this.btnSchliessen.TabIndex = 1;
            this.btnSchliessen.Text = "schliessen";
            this.btnSchliessen.UseVisualStyleBackColor = true;
            this.btnSchliessen.Click += new System.EventHandler(this.btnSchliessen_Click);
            // 
            // Form2
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(358, 305);
            this.Controls.Add(this.btnSchliessen);
            this.Controls.Add(this.lblAusgabe);
            this.Name = "Form2";
            this.Text = "Information";
            this.ResumeLayout(false);
        }

        private System.Windows.Forms.Button btnSchliessen;
        private System.Windows.Forms.Label lblAusgabe;

        #endregion
    }
}