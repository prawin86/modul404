﻿using System;
using System.Windows.Forms;

namespace StandardEreignisse
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        // Maus-Ereignisse auf dem Formular Klick
        private void Form1_Click(object sender, EventArgs e)
        {
            txtAusgabe.Text += "Klick" + Environment.NewLine;
        }

        // Maus-Ereignisse auf dem Formular Doppel-Klick
        private void Form1_DoubleClick(object sender, EventArgs e)
        {
            txtAusgabe.Text += "Doppelklick" + Environment.NewLine;
        }

        // Maus-Ereignisse auf dem Formular Maustaste gedrückt
        private void Form1_MouseDown(object sender, MouseEventArgs e)
        {
            switch (e.Button) // switch prüft welche Taste
            {
                case MouseButtons.Left:
                    txtAusgabe.Text += "linke Maustaste runter" + Environment.NewLine;
                    break;
                case MouseButtons.Right:
                    txtAusgabe.Text += "rechte Maustaste runter" + Environment.NewLine;
                    break;
                case MouseButtons.Middle:
                    txtAusgabe.Text += "mittlere Maustaste runter" + Environment.NewLine;
                    break;
            }
        }

        // Maus-Ereignisse auf dem Formular Maustaste losgelassen
        private void Form1_MouseUp(object sender, MouseEventArgs e)
        {
            switch (e.Button) // switch prüft welche Taste
            {
                case MouseButtons.Left:
                    txtAusgabe.Text += "linke Maustaste hoch" + Environment.NewLine;
                    break;
                case MouseButtons.Right:
                    txtAusgabe.Text += "rechte Maustaste hoch" + Environment.NewLine;
                    break;
                case MouseButtons.Middle:
                    txtAusgabe.Text += "mittlere Maustaste hoch" + Environment.NewLine;
                    break;
            }
        }

        // Tastatur-Ereignisse auf dem Formular: Maustaste losgelassen
        private void Form1_KeyDown(object sender, KeyEventArgs e)
        {
            txtAusgabe.Text += "Taste runter" + Environment.NewLine;
        }

        // Tastatur-Ereignisse auf dem Formular: Maustaste gedrückt
        private void Form1_KeyUp(object sender, KeyEventArgs e)
        {
            txtAusgabe.Text += "Taste hoch" + Environment.NewLine;
        }

        // Tastatur-Ereignisse auf dem Formular: Welche Taste gedrückt
        private void Form1_KeyPress(object sender, KeyPressEventArgs e)
        {
            txtAusgabe.Text += e.KeyChar + " Taste gedrückt" + Environment.NewLine;
        }
        
        // Info Taste
        private void button1_Click(object sender, EventArgs e)
        {
            var frm = new Form2(this);
            frm.Show();
        }
    }
}